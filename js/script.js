addEventListener('DOMContentLoaded', () => {

    const imagenes = ['img/1.jpg', 'img/2.jpg', 'img/3.jpg', 'img/4.jpg',
                     'img/5.jpg', 'img/6.jpg', 'img/7.jpg']
    let i = 0
    let img1 = document.querySelector('#img1')
    let img2 = document.querySelector('#img2')
    const progressBar = document.querySelector('#progress-bar')
    const divIndicadores = document.querySelector('#indicadores')
    let porcentaje_base = 100/imagenes.length
    let porcentaje_actual = porcentaje_base

    for (let index = 0; index < imagenes.length; index++) {
        let div = document.createElement('div')
        div.classList.add('circles')
        div.id = index
        div.dataset.divNumero = index + 1
        divIndicadores.appendChild(div)        
    }
    let intervalo
    const circulos = document.querySelectorAll('.circles')

    const main = (numero_imagen = 0, divPorcentaje = 1) => {
        if(typeof numero_imagen !== 'undefined') {
            i = numero_imagen
        }
        progressBar.style.width = `${ divPorcentaje !== 0 ? divPorcentaje * porcentaje_base : porcentaje_base}%`
        console.log(numero_imagen)
        img1.src = imagenes[numero_imagen]
        Array.from(circulos).forEach(cir => cir.classList.remove('resaltado'))
        circulos[numero_imagen].classList.add('resaltado')
        if (i < imagenes.length - 1)
            i++

        function slideShow () {
            console.log(i)
            img2.src = imagenes[i]
            const circulo_atual = Array.from(circulos).find(el => el.id == i)
            Array.from(circulos).forEach(cir => cir.classList.remove('resaltado'))
            circulo_atual.classList.add('resaltado')
            img2.classList.add('active')
            
            i++
            porcentaje_actual = i * porcentaje_base
            progressBar.style.width = `${porcentaje_actual}%`
            if(i == imagenes.length) {
                i = 0
                porcentaje_actual = porcentaje_base * 1
            }

            setTimeout(() => {
                img1.src = img2.src
                img2.classList.remove('active')
            }, 500)
        }
        intervalo = setInterval(slideShow, 4000)
    }
    main()

    Array.from(circulos).forEach(circulo => {
        circulo.addEventListener('click', event => {
            clearInterval(intervalo)
            main(+event.target.id, +event.target.dataset.divNumero)
        })
    })
})